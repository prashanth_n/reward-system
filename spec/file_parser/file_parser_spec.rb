require 'spec_helper'

describe FileParser do
  it 'should parse a file and return true if no error' do
    parser = DataParser.new(:file)
    expect(parser.parse("#{__dir__}/test_input_1")).to be(true)
  end
  it 'should return false when paresed and should have error message' do
    parser = DataParser.new(:file)
    expect(parser.parse("#{__dir__}/test_input_2")).to be(false)
    expect(parser.error_details.last).to match('Cannot parse line 2018-06-25 09:41 D blah')
  end
  it 'should sort the input data on cronological order' do
    parser = DataParser.new(:file)
    expect(parser.parse("#{__dir__}/test_input_3")).to be(true)
    expect(parser.input_data[4]).to eq(%w[recommends C D])
  end
  it 'should throw error if the required value is not in each array' do
    parser = DataParser.new(:file)
    expect(parser.parse("#{__dir__}/test_input_4")).to be(false)
    expect(parser.error_details.last).to eq("Cannot parse line 2018-06-23 09:41 B recommends")
  end
end
