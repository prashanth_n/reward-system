require 'points_calculator'
require 'yaml'
require 'file_parser'
require 'base_parser'
require 'data_parser'
require 'text_parser'
require 'parser_spec_helper'

RSpec.configure do
  include ParserSpecHelper
end
