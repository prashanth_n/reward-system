require 'spec_helper'

describe TextParser do
  let(:input_string) do
    '
                    2018-06-12 09:41 A recommends B         
      2018-06-14 09:41 B accepts
      2018-06-16 09:41 B recommends C

      2018-06-17 09:41 C accepts
      2018-06-19 09:41 C recommends D
      2018-06-23 09:41 B recommends D
      2018-06-25 09:41 D accepts

    '
  end
  let(:expected_output) do
    [
      ['recommends', 'A', 'B'],
      ['accepts', 'B'],
      ['recommends', 'B', 'C'],
      ['accepts', 'C'],
      ['recommends', 'C', 'D'],
      ['recommends', 'B', 'D'],
      ['accepts', 'D']
    ]
  end
  it 'should parse a file with trailing and starting spaces, new lines' do
    parser = DataParser.new(:text)
    expect(parser.parse(input_string)).to be(true)
    expect(parser.input_data).to match(expected_output)
  end
end
