module ParserSpecHelper
  def parse_file(file_name)
    parser = DataParser.new(:file)
    parser.parse(file_name)
  end
end
