require 'spec_helper'
describe PointsCalculator do
  let(:test_data) do
    file_path = File.dirname(File.expand_path(__FILE__)) + '/input_data.yml'
    @test_data ||= YAML.safe_load(File.open(file_path))
  end
  let(:calulate_points) do
    ->(test_case) { PointsCalculator.new(test_case['input']).calculate_points! }
  end
  it 'should not give points for the referer without invitee accepted' do
    expect(calulate_points[test_data['case_0']]).to eq('A' => 0.5, 'B' => 1.0)
  end
  it 'should add points to referer irrespective of referer acceptance' do
    expect(calulate_points[test_data['case_0']]).to eq('A' => 0.5, 'B' => 1.0)
  end
  it 'should not add points for an already referered customer' do
    expect(calulate_points[test_data['case_0']]).to eq('A' => 0.5, 'B' => 1.0)
  end
  it 'should not show customer with zero points' do
    expect(calulate_points[test_data['case_0']]['C']).to eq(nil)
  end
  it 'different input cases' do
    test_data.each do |case_id, data|
      puts "testing for #{case_id}"
      expect(calulate_points[data]).to eq(data['output'])
    end
  end
end
