require 'sinatra'
require_relative 'reward_system'

get '/' do
  erb :index
end
post '/calculate' do
 @response_data = RewardSystem.compute(file_path: params['file_path'], input_data: params['input_data'])
 erb :final_points
end
