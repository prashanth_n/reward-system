require_relative 'base_parser'

# parsers the input given from web
class TextParser < BaseParser
  def parse(input)
    input.lines.each do |line|
      line.strip!
      next if line && line.empty?

      unless validate_and_parse(line)
        @error_details << "Cannot parse line #{line}"
        return false
      end
    end
    remove_date!
    true
  end
end
