# details of the customers include references to parent referer and child references
class Customer
  attr_accessor :points, :referer, :accepted, :child_references
  def initialize(referer = nil)
    self.referer = referer
    self.points = 0.0
    self.accepted = false
    self.child_references = []
  end

  def add_reference(child_reference)
    return child_references if child_reference.nil?

    child_references << child_reference
  end
end
