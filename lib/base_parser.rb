require 'date'

# core parsing logic is done here
class BaseParser
  attr_reader :error_details, :input_data

  LINE_PARSER_REGEX = /^(\d{4}-\d{2}-\d{2} \d{2}:\d{2})\s+(\S+)\s+(recommends|accepts)\s*(\S*)$/.freeze

  def initialize
    @input_data = []
    @error_details = []
  end

  # rubocop:disable Metrics/AbcSize
  # rubocop:disable Metrics/CyclomaticComplexity
  # rubocop:disable Metrics/PerceivedComplexity
  def validate_and_parse(line)
    line.strip!
    match_data = LINE_PARSER_REGEX.match(line)
    return false if match_data.nil?

    begin
      time = DateTime.strptime(match_data[1], '%Y-%m-%d %H:%M').to_time
      recommender_name = match_data[2].strip
      recommendee_name = match_data[4].strip
      operation = match_data[3]
      return false unless %w[recommends accepts].include?(match_data[3])
      return false if recommender_name.empty? || time.nil?
      return false if operation == 'recommends' && recommendee_name.empty?

      arr = [time, match_data[3], recommender_name]
      arr << recommendee_name if operation == 'recommends'
      @input_data << arr
      @input_data.sort_by!(&:first)
    rescue StandardError => e
      @error_details << e
      return false
    end
    true
  end
  # rubocop:enable Metrics/AbcSize
  # rubocop:enable Metrics/CyclomaticComplexity
  # rubocop:enable Metrics/PerceivedComplexity

  def remove_date!
    @input_data.each(&:shift)
  end
end
