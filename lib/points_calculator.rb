require_relative 'customer'

# calculates the final points for each customer
class PointsCalculator
  attr_accessor :customers
  def initialize(input_data)
    @input_data = input_data
    @customers = {}
  end

  def calculate_points!
    @input_data.each do |data|
      case data[0].to_s
      when 'recommends'
        recommendation(data[1].to_s.strip, data[2].to_s.strip)
      when 'accepts'
        acceptance(data[1])
      end
    end
    calculate_referer_points
    final_points
  end

  def final_points
    final_hash = Hash[*customers.map { |name, details| [name, details.points] }.flatten]
    final_hash.reject { |_, points| points.zero? }
  end

  # function adds recomender and recomendee to hash if not present and sets the right references
  def recommendation(recomender_name, recomendee_name)
    recomender_details = (@customers[recomender_name] ||= Customer.new)
    recomendee_details = @customers[recomendee_name]
    if @customers[recomendee_name].nil?
      @customers[recomendee_name] = Customer.new(recomender_details)
      recomender_details.add_reference(@customers[recomendee_name])
    elsif recomendee_details.referer.nil?
      return puts "cyclic reference #{recomender_name} -> #{recomendee_name}" if cyclic_reference?(recomendee_details, recomender_details)

      recomendee_details.referer = recomender_details
      recomender_details.add_reference(recomendee_details)
    end
  end

  # checks for cyclic reference in the recomendation input
  def cyclic_reference?(recomendee, recomender)
    cyclic_reference = false
    while recomender
      if recomender == recomendee
        cyclic_reference = true
        break
      end
      recomender = recomender.referer
    end
    cyclic_reference
  end

  def acceptance(customer_name)
    customer_name = customer_name.to_s.strip
    return puts "#{customer_name} is not recommended by anyone" unless @customers[customer_name]

    @customers[customer_name].accepted = true
  end

  # points calculation and updation for each customer is done in O(n) complexity
  def update_referer_points(customer)
    return if customer.nil?

    customer.points = customer.child_references.sum do |reference|
      update_referer_points(reference)
      (reference.points / 2) + (reference.accepted ? 1.0 : 0.0)
    end
  end

  def calculate_referer_points
    @customers.each do |_, details|
      update_referer_points(details) if details.referer.nil?
    end
  end
end
