require_relative 'base_parser'

# File parser to pass the input file
class FileParser < BaseParser
  def parse(file_location)
    unless File.exist?(file_location.to_s)
      @error_details = 'File does not exists'
      return false
    end
    File.open(file_location) do |file|
      file.each_line.lazy.each do |line| # lazy file loading instead of eager load which keeps file in memory
        line.strip!
        next if  line && line.empty?

        unless validate_and_parse(line)
          @error_details << "Cannot parse line #{line}"
          return false
        end
      end
    end
    remove_date!
    true
  end
end
