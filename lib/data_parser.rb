require 'forwardable'
require_relative 'file_parser'
require_relative 'text_parser'
# main class to parse different files
class DataParser
  extend Forwardable
  PARSER_CLASS_MAP = {
    file: FileParser,
    text: TextParser
  }.freeze

  def_delegators :@parser, :parse, :error_details, :input_data

  def initialize(parser)
    parser_class = PARSER_CLASS_MAP[parser.to_sym]
    raise 'Cannot find the right Parser' unless parser_class

    @parser = parser_class.new
  end
end
