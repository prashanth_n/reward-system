# README

## TechStack Choice
I have decided to build this application on Sinatra.Intially I thought of building the application on Rails as I had not worked and hence no idea of Sinatra
After having a quick reseach on it, It made more sense to build it using Sinatra than on Rails as Rails brings in an unnecessary complexity to the system

## Files and Classes 
* DataParser - Base parser class
* FileParser - when a file parsed class which will parse and make a array structure
* PointsCalculator - accepts array of details and gives out a hash of points for each customer
* CustomerDetails - holds details like points and aceptence status. It has a two way reference, to parent referer and child references
* app.rb - a sintara file
* reward_system - a wrapper class to calculate points
* input_file - a simple text file to process input

## Points calculation logic
	Each customer reference is done by adding the name of the customer as a key in the hash. This way the lookup complexity is O(1)
	The structure is not one big tree but rather multiple trees as there can be customers without any referal but he can refer other customers
### Algorithm 
 	Iterate each customer starting from leaf level and traverse to the root referer
 	In each Node calculate the points -
 	 points = half of the sum of all his child reference points + one point each for child reference if he has accepted the invitation
 	This has a time complexity of O(n)
 	This is better that traversing parent nodes and updating (1/2)^n points for every node present which will have a complexity of O(n^2)  

## How to Run?
install sinatara gem if not installed
```
gem install sinatra
```
start the sinatra app by running
```
ruby reward_system.rb
```
puma web service will be started at port 4567

now go to brower and enter the localhost url
```
http://localhost:4567/
```
You can either choose to enter the file path or directly the input text and submit
you will be getting the points for each customers
