Dir[File.dirname(__FILE__) + '/lib/*.rb'].each { |file| require file }

# Wrapper Class to calculate points
class RewardSystem
  def self.compute(input_details)
    input_data = input_details[:input_data].strip
    data_type = input_data.empty? ? :file : :text
    data_parser = DataParser.new(data_type)
    file = input_data.empty? ? input_details[:file_path] : input_data
    return "Error: #{data_parser.error_details}" unless data_parser.parse(file)

    points_calculator = PointsCalculator.new(data_parser.input_data)
    points_calculator.calculate_points!
  end
end
